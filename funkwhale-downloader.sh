#!/usr/bin/env bash
#
# Usage example:
# ./funkwhale-downloader.sh https://instance.com/library/playlists/28
# or
# ./funkwhale-downloader.sh https://instance.com/library/albums/8519/

playlist_url=${1%/}
playlist_id=`echo $playlist_url \
  | sed -E 's|(https?://[^/]+/library/[a-z]+/([0-9]+))|\2|'`
base_url=$(echo $playlist_url | cut -d/ -f 1-3)
if $(echo $playlist_url | grep -q albums); then
  track=''
  playlist_type=album;
  url=$base_url/api/v1/tracks/?album=$playlist_id
else
  track='.track'
  playlist_type=playlist;
  url=$base_url/api/v1/playlists/$playlist_id/tracks/
fi

echo " "
echo Downloading from $playlist_type url: $1
echo API call url: $url
echo " "

echo Downloading files...
jq_filter=".results[]$track.listen_url"
for i in $(curl -s $url | jq -r $jq_filter); do
  wget -c --content-disposition $base_url$i;
done;

echo Converting flac to mp3...
for f in *.flac; do
  flac -cd "$f" | lame -b 320 - "${f%.*}".mp3;
done;

echo Removing flac files...
rm *.flac

echo Done.
exit